import argparse as ap
from utils.sensors import SensorManagement
from utils.rules import RulesManagement
from pprint import pprint
import time
from datetime import datetime
rm = RulesManagement()

parser = ap.ArgumentParser(description='Checking Rules')
parser.add_argument('-s', '--start', type=float, help='hours before start', default=24)
parser.add_argument('-e', '--end', type=float, help='hours before end', default=0)
parser.add_argument('-m', '--museum', type=int, help='mudeum id to check', default=1)
parser.add_argument('-l', '--log', type=str, help='log file', default="")
args = parser.parse_args()

now = int(time.time())
starttime = now - 3600*args.start
endtime = now - 3600*args.end

actions = rm.checkRulesMuseum(args.museum, starttime, endtime, args.log)