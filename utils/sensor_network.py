import socket
import select
import time
import json
import threading
import sys
from pprint import pprint
from pymongo import MongoClient
from utils.utils import translate
from utils.sensors import SensorManagement


class SocketServer:
    
    def __init__(self, host='localhost', port=10000, verbose=True, max_sockets=5, fixed=4):
        threading.Thread.__init__(self)
        self.verbose = verbose
        self.max_sockets = max_sockets
        self.host = host
        self.port = port
        self.fixed = fixed
        # Create a TCP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
        # Bind the socket to the port
        if self.verbose:
            print('Opening server socket (host {}, port {})'.format(self.host, self.port))
        self.sock.bind((host, port))
        self.sock.listen(self.max_sockets)
        self.sock_threads = []
        self.counter = 0 # Will be used to give a number to each thread
        self.mgt = SensorManagement() 
        self.lock = threading.Lock()
        
    def close(self):
        if self.verbose:
            print('Closing server socket (host {}, port {})'.format(self.host, self.port))
        for thr in self.sock_threads:
            thr.stop()
            thr.join()
        if self.sock:
            self.sock.close()
            self.sock = None

            
    def start(self):
        if self.verbose:
            print('Starting socket server (host {}, port {})'.format(self.host, self.port))
        self.__stop = False
        while not self.__stop:
            self.sock.settimeout(1)
            try:
                client_sock, client_addr = self.sock.accept()
            except socket.timeout:
                client_sock = None

            if client_sock:
                client_thr = SocketServerThread(client_sock, client_addr, self.counter,self.verbose, self.fixed, self.mgt, self.lock)
                self.counter += 1
                self.sock_threads.append(client_thr)
                client_thr.start()
        self.close()

    def stop(self):
        self.__stop = True
        
        
class SocketServerThread(threading.Thread):
    
    def __init__(self, client_sock, client_addr, number, verbose, fixed, mgt, lock):
        threading.Thread.__init__(self)
        self.client_sock = client_sock
        self.client_addr = client_addr
        self.number = number
        self.verbose = verbose
        self.fixed = fixed
        self.mgt = mgt
        self.lock = lock
        
    def send_ack(self, sock):
        txt = "ACK"
        sock.sendall(txt.encode('utf-8'))
    
    def read(self, sock):
        msg_received = ""
        while True:
            try:
                content = sock.recv(255)
            except:
                break
            if len(content) ==0:
                break
            msg_received += content.decode('utf-8')
        return msg_received           
        
    def process(self, message):
        try:
            decoded = json.loads(message)
        except:
            print ('[Thr {}] Message not in correct json format'.format(self.number))
            return
        #pprint (decoded)
        if not isinstance(decoded, list):   #if not
            decoded = [decoded]
        for row in decoded:
            uid = row["id"]
            param = row["param"]
            del row["id"]
            self.lock.acquire()
            try:
                self.mgt.insertSensorData(uid, row, param)
                print('[Thr {}] Sensor data stored ad MongoDB'.format(self.number))
            finally:
                self.lock.release()
        
    def run(self):
        if self.verbose:
            print("[Thr {}] SocketServerThread starting with client {}".format(self.number, self.client_addr))
        self.__stop = False
        while not self.__stop:
            if self.client_sock:
                # Check if the client is still connected and if data is available:
                try:
                    rdy_read, rdy_write, sock_err = select.select([self.client_sock,], [self.client_sock,], [], 5)
                except select.error as err:
                    print('[Thr {}] Select() failed on socket with {}'.format(self.number,self.client_addr))
                    self.stop()
                    return
                
                if len(rdy_read) > 0:
                    msg_received = self.read(self.client_sock)
                    if len(msg_received)>0:
                        if self.verbose:
                            print('[Thr {}] Received: {}'.format(self.number, msg_received.rstrip()))
                        self.send_ack(self.client_sock)
                        self.process(msg_received)
                    else:
                        self.stop()
            else:
                if self.verbose:
                    print("[Thr {}] No client is connected, SocketServer can't receive data".format(self.number))
                self.stop()
        self.close()

    def stop(self):
        self.__stop = True

    def close(self):
        if self.client_sock:
            if self.verbose:
                print('[Thr {}] Closing connection with {}'.format(self.number, self.client_addr))
            self.client_sock.close()
            
            
class SocketClient:
    
    def __init__(self, host='localhost', port=10000, fixed=4, id='FFFF'):
        self.host = host
        self.port = port
        self.fixed = fixed
        self.uid = id
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((host, port))

    def read(self):
        msg_received = ""
        while True:
            try:
                content = self.sock.recv(255)
            except:
                break
            if len(content) ==0:
                break
            msg_received += content.decode('utf-8')
        return msg_received  
    
    def send(self, message):
        try:
            #send message
            msg_sending = translate(message)
            print ("Sending:", message)
            self.sock.sendall(msg_sending.encode('utf-8'))
            msg_received = self.read()
            print ("Received:", msg_received)
        finally:
            print ('closing socket')
            self.sock.close()  
                
    def send_json(self, param, value, timestamp=0):
        if timestamp==0:
            timestamp = int(time.time())
        data = json.dumps({'id': self.uid, 'timestamp': timestamp, 'param': param, 'value': value})
        self.send(data)