import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from datetime import datetime
from textmagic.rest import TextmagicRestClient
import pywemo

class SensorEmail:
    def __init__(self):
        self.port = 465  # For SSL
        self.smtp_server = "smtp.gmail.com"
        self.sender_email = "kmichalakis.develop@gmail.com"
        self.receiver_email = "kmichalakis@gmail.com"
        self.password = "grunge6!"

    def send_sensor(self, receiver_email, subject, sensor, crit, rule):
        message = MIMEMultipart("alternative")
        message["Subject"] = subject
        message["From"] = self.sender_email
        message["To"] = receiver_email
        
        starttime = datetime.fromtimestamp(crit[0][1]).strftime('%Y-%m-%d %H:%M:%S')
        endtime = datetime.fromtimestamp(crit[0][2]).strftime('%Y-%m-%d %H:%M:%S')

        # Create the plain-text and HTML version of your message
        text = """\
Important Message from Middleware
Sensor: """+sensor+""" has shown sub normal bevahiour
Rule: """+rule+ """
Time Period: from """+starttime+ """ to """+endtime
        
        html = """\
        <html>
          <body>
            <h1>Important Message from Middleware</h1>
            <p>Sensor: <b>"""+sensor+"""</b> has shown sub normal bevahiour</p>
            <p><b>Rule:</b> """+rule+"""</p>
            <p><b>Time Period:</b> from <i>"""+starttime+"""</i> to <i>"""+endtime+"""</i></p>
        """
        
        for cr in crit:
            text += "\nParam: " + cr[0] + " Value: " + str(cr[3])
            html += "<p><b>Param:</b> " + cr[0] + " <b>Value:</b> " + str(cr[3]) + "</p>"
            
        html += "</body></html>"

        # Turn these into plain/html MIMEText objects
        part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html")

        # Add HTML/plain-text parts to MIMEMultipart message
        # The email client will try to render the last part first
        message.attach(part1)
        message.attach(part2)
        
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(self.smtp_server, self.port, context=context) as server:
            server.login(self.sender_email, self.password)
            server.sendmail(
                self.sender_email, receiver_email, message.as_string()
            )

class SensorSMS:
    def __init__(self):
        self.username = "konstantinosmichalak"
        self.token = "kQsJ1cXdlouB2KsA9lsfn2aPPRcUjD"
        self.client = TextmagicRestClient(self.username, self.token)
    
    def send_sensor(self, receiver_phone, sensor, crit, rule):
        starttime = datetime.fromtimestamp(crit[0][1]).strftime('%Y-%m-%d %H:%M:%S')
        endtime = datetime.fromtimestamp(crit[0][2]).strftime('%Y-%m-%d %H:%M:%S')
        text = "Alert! Sensor: "+sensor+" "+rule+" From """+starttime+ """ to """+endtime
        for cr in crit:
            text += " Param:" + cr[0] + " Value:" + str(cr[3])
        #message = self.client.messages.create(phones=receiver_phone, text=text)
        #TODO use a free way to send sms
        
class wemoLamp:
    def __init__(self, id):
        self.id = id
        self.devices = pywemo.discover_devices()
    
    def action(self, mode):
        lamp = self.devices[0].Lights[self.id]
        if mode == 0:
            lamp.turn_on()
        elif mode == 1:
            lamp.turn_off()
    
class Action:
    def __init__(self):
        return
    
    def run(self, action, id):
        if action==1:  #call
            #TODO open when ready to send email
            s = "   Calling " + id
        elif action==2:  #SMS
            #TODO open when ready to send SMS
            #sms = SensorSMS()
            #sms.send_sensor(action["target"], sensor, crit, rule)
            s = "   Sending SMS to " + id
        elif action==3:   #send email
            #TODO open when ready to send email
            #email = SensorEmail()
            #email.send_sensor(action["target"], "Middleware Alert", sensor, crit, rule)
            s = "   Email sent to "+ id
        elif action==4:  #smart object on
            wemo = wemoLamp(id)
            wemo.action(0) #turn on
            s = "   Smart Object "+ id + "turned on"
        elif action==5:  #smart object off
            wemo = wemoLamp(id)
            wemo.action(1) #turn off
            s = "   Smart Object "+ id + "turned off \n"
        return s
           
        