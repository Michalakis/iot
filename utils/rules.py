from pymongo import MongoClient
from utils.sensors import SensorManagement
from utils.museum import MuseumManagement
from utils.actions import Action
from pprint import pprint
from datetime import datetime
import time
import numpy as np
import collections

class RulesManagement:
    def __init__(self):
        self.db = MongoClient('mongodb+srv://kmichalakis:GondoliN6!@cluster0-vaaym.mongodb.net/test?retryWrites=true&w=majority').rules 
        self.logfile = 0
        
    def addRule(self, sensor, rule, action, text):
        self.db.rules.insert_one({'sensor': sensor, 'text':text, 'rule':rule, 'action':action})
        
    def deleteRule(self, sensor, ruleID):
        self.db.rules.delete_one({'sensor': sensor, '_id':ruleID})
    
    def updateRule(self, sensor, ruleID, rule):
        self.db.rules.update_one({'sensor': sensor, '_id':ruleID}, { "$set": { "rule": rule } })
        
    def getRules(self, sensor):
        # output: list of rules for the sensor
        return list(self.db.rules.find({"sensor":sensor},{"_id":1, "rule":1, "action":1, "text":1}))
    
    def checkRule(self, sensor, rule_, starttime, endtime):
        # input: sensor, param to check, starttime, endtime
        # ouput: list of actions required
        rule = Rules()
        rule.init(rule_)
        return eval(rule.resolve(sensor, starttime, endtime)), rule.critical
        
    def checkRulesSensor(self, sensor, starttime, endtime):
        print ("Checking rules for Sensor:", sensor)
        rules = self.getRules(sensor)
        results = []
        for rule in rules:
            res = self.checkRule(sensor, rule["rule"], starttime, endtime)
            if res[0]:
                action = self.getAction(rule["action"])
                self.performAction(action, sensor, res[1], rule["text"])
                result=[]
                result.append(sensor)
                result.append(action["message"])
                results.append(result)
        sm = SensorManagement()
        return results
    
    def checkRulesRoom(self, room, starttime, endtime, log = ""):
        print ("Checking rules for Room:", room)
        if log!="":
            self.logfile = open(log, "a")
            starttime_text = datetime.fromtimestamp(starttime).strftime('%Y-%m-%d %H:%M:%S')
            endtime_text = datetime.fromtimestamp(endtime).strftime('%Y-%m-%d %H:%M:%S')
            self.logfile.write("Checking from " + str(starttime_text) + " to " + str(endtime_text) + "\n")
        if self.logfile!=0:   
            self.logfile.write("Checking rules for Room:" + str(room) + "\n")
        results = []
        sm = SensorManagement()
        sensors = sm.getSensors(room)
        for sensor_obj in sensors:
            sensor = sensor_obj["id"]
            rules = self.getRules(sensor)
            for rule in rules:
                res = self.checkRule(sensor, rule["rule"], starttime, endtime)
                if res[0]:
                    action = self.getAction(rule["action"])
                    self.performAction(action, sensor, res[1], rule["text"])
                    result=[]
                    result.append(sensor)
                    result.append(action["message"])
                    results.append(result)
        print ("----------------------")
        print ("Total actions:", len(results))
        print ("======================")
        if self.logfile!=0:
            self.logfile.write("----------------------\n")
            self.logfile.write("Total actions:" +  str(len(results)) + "\n")
            self.logfile.write("======================\n")
        return results
    
    
    def checkRulesMuseum(self, museum, starttime, endtime, log=""):
        starttime_text = datetime.fromtimestamp(starttime).strftime('%Y-%m-%d %H:%M:%S')
        endtime_text = datetime.fromtimestamp(endtime).strftime('%Y-%m-%d %H:%M:%S')
        print("Checking from " + str(starttime_text) + " to " + str(endtime_text))
        print ("Checking rules for Museum:", museum)
        print ("----------------------")
        if log!="":
            self.logfile = open(log, "a")
            self.logfile.write("Checking from " + str(starttime_text) + " to " + str(endtime_text) + "\n")
            self.logfile.write("Checking rules for Museum:" + str(museum) + "\n")
            self.logfile.write(("----------------------\n"))
        results = []
        mm = MuseumManagement(museum)
        rooms = mm.getRooms()
        for room in rooms:
            result = self.checkRulesRoom(room["room"], starttime, endtime)
            results.extend(result)
            
        if log!="":
            self.logfile.close()
            self.logfile = 0
        return results
    
    def addFluctuation(self, sensor, text, param, fl, tp, cl):
        self.db.fluctuations.insert_one({'sensor': sensor, 'text':text, 'param':param, 'fl':fl, 'type':tp, 'class':cl})
        
    def getFluctuations(self, sensor):
        # output: list of fluctuation rules for the sensor
        return list(self.db.fluctuations.find({"sensor":sensor}))
        
    def checkShortFluctuationsSensor(self, sensor, days):
        sm = SensorManagement()
        now = int(time.time())
        #get all fluctuanion rules
        fls_all = self.getFluctuations(sensor)
        #split them according to param
        result = collections.defaultdict(list)
        for d in fls_all:
            result[d['param']].append(d)
        fls_ordered = list(result.values())
        fl_class = []
        for fls in fls_ordered:
            print ("Checking:", fls[0]["param"])
            starttime = now - 3600*24
            endtime = now
            max_fluct = 0
            for i in range(days):
                values_array = sm.getSensorData(sensor, fls[0]["param"], starttime, endtime)
                if len(values_array)==0:
                    continue
                values, timestamps = zip(*values_array)
                fluctuation = max(values) - min(values)
                if fluctuation>max_fluct:
                    max_fluct = fluctuation
                endtime = starttime
                starttime = now - 3600*24*(i+2)
            print ("Fluctuation:", max_fluct)
            best_class=100
            for fl in fls:
                if fl["type"]!=1:  #short term only
                    continue
                if max_fluct <= fl["fl"] and fl["class"]<best_class:
                    best_class = fl["class"]
            print ("best class for ", fls[0]["param"], " is ", best_class)
                           

    def checkSeasonalFluctuationsSensor(self, sensor, starttime, endtime):
        sm = SensorManagement()
        now = int(time.time())
        #get all fluctuanion rules
        fls_all = self.getFluctuations(sensor)
        #split them according to param
        result = collections.defaultdict(list)
        for d in fls_all:
            result[d['param']].append(d)
        fls_ordered = list(result.values())
        fl_class = []
        for fls in fls_ordered:
            print ("Checking:", fls[0]["param"])
            values_array = sm.getSensorData(sensor, fls[0]["param"], starttime, endtime)
            values, timestamps = zip(*values_array)
            fluctuation = max(values) - min(values)
            print ("Fluctuation:", fluctuation)
            best_class=100
            for fl in fls:
                if fl["type"]!=2:  #seasonal only
                    continue
                if fluctuation <= fl["fl"] and fl["class"]<best_class:
                    best_class = fl["class"]
            print ("best class for ", fls[0]["param"], " is ", best_class)
                           
  
    
    def addAction(self, action, message, type, target):
        num=self.db.actions.count({"action":action})
        if num==0:
            self.db.actions.insert_one({'action': action, 'message':message, 'type':type, 'target':target})
        else:
            print ("Action ID existing")
            
    def getActions(self):
        # output: list of all actions
        return list(self.db.actions.find({},{"_id":1, "action":1, "message":1}))
    
    def getAction(self, action):
        return self.db.actions.find_one({"action":action})
    
    def performAction(self, action, sensor, crit, rule):
        print ("Action required for sensor " + sensor + ": " + action["message"])
        print ("   Parameter: " + crit[0][0] + ", value: " + str(crit[0][3]) + ", rule: " + rule)
        if self.logfile!=0:
            self.logfile.write("Action required for sensor " + sensor + ": " + action["message"] + "\n")
            self.logfile.write("   Parameter: " + crit[0][0] + ", value: " + str(crit[0][3]) + ", rule: " + rule + "\n")
        act = Action()
        s = act.run(action["type"], action["target"])
        print (s)
        if self.logfile!=0:
            self.logfile.write(s)
    
class Rules:
    def __init__(self, init=""):
        self.ex = []
        if init!="":
            self.addRule(init)
        self.sensors = SensorManagement()
        self.critical = []
            
    def init(self, init_ex):
        self.ex = init_ex
    
    def clear(self):
        self.ex = []
    
    def last(self):
        if len(self.ex)>0:
            return self.ex[-1]["type"], self.ex[-1]["op"]
        return 0, 0;
    
    def addSymbol(self, op):
        if op!="(" and op!=")":
            print ("Bad symbol")
            return
        last_type, last_op = self.last()
        #not allowed types: True(  )( ()  and) )
        if (op=="(" and (last_op==")" or last_type==1)) or (op==")" and (last_op=="(" or last_type==2 or last_type==0)):
            print ("Bad syntax")
            return
        entry = {"type":3, "op":op}
        self.ex.append(entry)
    
    def addOperator(self, op):
        if op!="and" and op!="or" and op!="not":
            print ("Bad operator")
            return
        last_type, last_op = self.last()
        #not allowed orand  (and  and
        if last_type==2  or last_type==0 or last_op=="(":
            print ("Bad syntax")
            return
        entry = {"type":2, "op":op}
        self.ex.append(entry)
        
    def addRule(self, rule):
        if not ("param" in rule and "threshold" in rule and "comparison" in rule):
            print ("Bad rule")
            return
        last_type, last_op = self.last()
        #not allowed )True TrueFalse
        if last_type==1  or last_op==")":
            print ("Bad syntax")
            return
        keys = ["param", "threshold", "comparison"]
        rule_clear = { key: rule[key] for key in keys }
        entry = {"type":1, "op":rule_clear}
        self.ex.append(entry)
    
    def print(self):
        pprint(self.ex)
        
    def resolveOne(self, sensor, rule, starttime, endtime):
        param = rule["param"]
        threshold = rule["threshold"]
        comparison = rule["comparison"]
        values = self.sensors.getSensorData(sensor, param, starttime, endtime)
        ret = "False"
        val = None
        for value_item in values:
            value = value_item[0]
            if rule["comparison"] == "gt" and value>rule["threshold"]:
                ret = "True"
                if val==None or value>val:
                    val=value
            elif rule["comparison"] == "gte" and value>=rule["threshold"]:
                ret = "True"
                if val==None or value>val:
                    val=value
            elif rule["comparison"] == "eq" and value==rule["threshold"]:
                ret = "True"
                val=value
            elif rule["comparison"] == "lte" and value<=rule["threshold"]:
                ret = "True"
                if val==None or value<val:
                    val=value
            elif rule["comparison"] == "lt" and value<rule["threshold"]:
                ret = "True"
                if val==None or value<val:
                    val=value
        if ret:
            crit=[]
            crit.append(param)
            crit.append(starttime)
            crit.append(endtime)
            crit.append(val)
            self.critical.append(crit)
        return ret
    
    def resolve(self, sensor, starttime, endtime):
        ret = ""
        for op in self.ex:
            if op["type"]==2 or op["type"]==3:
                ret += op["op"]+" "
            elif op["type"]==1:
                ret += self.resolveOne(sensor, op["op"], starttime, endtime)+" "
        return ret

    
    