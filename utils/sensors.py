import time
from pymongo import MongoClient

class SensorManagement:
    def __init__(self, room=0):
        self.db = MongoClient('mongodb+srv://kmichalakis:GondoliN6!@cluster0-vaaym.mongodb.net/test?retryWrites=true&w=majority').sensors
        self.room = room
    
    def addSensorID(self, uid):
        if self.checkSensorID(uid):
            return
        self.db.sensor_list.insert_one({'id': uid, 'param':[],'room':self.room})
        print ("Sensor", uid,"added")
        
        
    def addSensorParam(self, uid, param):
        if self.checkSensorParam(uid, param):
            return
        self.db.sensor_list.update_one(
           { "id": uid },
           { "$push": { "param": param } }
        )
        print ("Param", param, "added to sensor", uid)
    
    def checkSensorID(self, uid):
        if self.db.sensor_list.count_documents({'id': uid})>0:
            return True
        return False
    
    def checkSensorParam(self, uid, param):
        if self.db.sensor_list.count_documents({'id': uid, "param": param})>0:
            return True
        return False
    
    def insertSensorData(self, uid, msg, param):
        self.addSensorID(uid)
        self.addSensorParam(uid, param)
        sensorID = "sensor" + uid
        if 'timestamp' not in msg:
            msg["timestamp"] = int(time.time())
        self.db[sensorID].insert_one(msg)
        
    def getSensors(self, room):
        return list(self.db["sensor_list"].find({'room':room},{"_id":0, "id":1, "param":1}))
    
    def getSensorData(self, uid, param, starttime, endtime):
        if self.checkSensorID(uid)== False:
            return 0
        sensorID = "sensor" + uid
        result = self.db[sensorID].find({"param":param, "timestamp":{ "$gte" :  starttime, "$lte" : endtime}},{ "_id": 0 }).sort("timestamp")
        lst = []
        for item in result:
            el = [item["value"], item["timestamp"]]
            lst.append(el)
        return lst
    
    def getSensorParams(self, uid):
        return self.db["sensor_list"].find_one({"id":uid},{"_id":0, "param":1})["param"]
    
    def getSensorDataAll(self, uid, starttime, endtime):
        params = self.getSensorParams(uid)
        results = []
        for param in params:
            results.append(self.getSensorData(uid, param, starttime, endtime))
        return params, results