#include "connection.h"

void setup() {
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Serial OK");
  if (!ENV.begin()) {
    Serial.println("Failed to initialize MKR ENV shield!");
    while (1);
  }
  Serial.println("ENV OK");
  connect_to_wifi();
  //printWifiStatus();
  send_to_server();
}

void loop() {

}