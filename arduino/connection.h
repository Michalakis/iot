#include <WiFiNINA.h>
#include <ArduinoJson.h>
#include <Arduino_MKRENV.h>

const char ssid[] = SECRET_SSID;    // Network SSID (name)
const char pass[] = SECRET_PASS;    // Network password (use for WPA, or use as key for WEP)
const char SENSOR_ID[] = "123FF22";
const char server[] = "192.168.11.39";
const int port = 10001;
int status = WL_IDLE_STATUS;

void connect_to_wifi() {
  String fv = WiFi.firmwareVersion();
  if (fv < "1.0.0") {
    Serial.println("Please upgrade the firmware");
  }

  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) {
    Serial.print("[WiFi] Connecting to: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network:
    status = WiFi.begin(ssid, pass);

    // wait 10 seconds for connection:
    delay(100);
  }

  // you're connected now, so print out the data:
  Serial.println("[WiFi] Connected");
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void get_sensor_data(float* t, float* h){
  Serial.println("Sensing parameters");
  // read all the sensor values
  float temperature = ENV.readTemperature();
  float humidity    = ENV.readHumidity();
  float pressure    = ENV.readPressure();
  float illuminance = ENV.readIlluminance();
  float uva         = ENV.readUVA();
  float uvb         = ENV.readUVB();
  float uvIndex     = ENV.readUVIndex();

  // print each of the sensor values
  Serial.print("Temperature = ");
  Serial.print(temperature);
  Serial.println(" °C");

  Serial.print("Humidity    = ");
  Serial.print(humidity);
  Serial.println(" %");

  Serial.print("Pressure    = ");
  Serial.print(pressure);
  Serial.println(" kPa");

  Serial.print("Illuminance = ");
  Serial.print(illuminance);
  Serial.println(" lx");

  Serial.print("UVA         = ");
  Serial.println(uva);

  Serial.print("UVB         = ");
  Serial.println(uvb);

  Serial.print("UV Index    = ");
  Serial.println(uvIndex);
  *t = temperature;
  *h = humidity;
}

void sendToClient(WiFiClient client){
  float temperature, humidity;
  get_sensor_data(&temperature, &humidity);
  
  Serial.println("Preparing json");
  StaticJsonDocument<200> doc, doc1, doc2;
  JsonArray array = doc.to<JsonArray>();
  
  doc1["id"] = SENSOR_ID;
  doc1["param"] = "humidity";
  doc1["value"] = humidity;
  doc2["id"] = SENSOR_ID;
  doc2["param"] = "temperature";
  doc2["value"] = temperature;
  
  array.add(doc1);
  array.add(doc2);
  Serial.println("Sending json");
  serializeJson(doc, client);

}


void listenToClient(WiFiClient client)
{
  Serial.println("listening to server");
  int i = 0;
  char * res = (char *) malloc(sizeof(char) * 255);
  while(client.available())
  {
    res[i++] = client.read();
  }
  Serial.println("finished listening to server");
  Serial.println(res);
}

// this method makes a HTTP connection to the server:
void send_to_server() {
  WiFiClient client;
  if (client.connect(server, port)) {
    Serial.println("connected to server");
    sendToClient(client);
    listenToClient(client);
    client.stop();
  }
  else{
    Serial.println("error connected to server");
  }
  Serial.println("finished connection with server");
}



