import argparse as ap
import time
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
from sklearn.ensemble import IsolationForest
import pickle
from utils.sensors import SensorManagement

plt.rcParams['backend'] = "Qt4Agg"
sm = SensorManagement()
rng = np.random.RandomState(42)

parser = ap.ArgumentParser(description='Fitting the model')
parser.add_argument('-s', '--sensor', type=str, help='Sensor ID', default="0000000")
parser.add_argument('-d', '--days', type=int, help='number of days back for data collection', default=7)
args = parser.parse_args()

      
# Load data for fit
end = int(time.time())
start = end - 3600*24*args.days
params, results = sm.getSensorDataAll(args.sensor, start, end)

# fix the timestamps to minutes in the day
for i  in range(len(params)):
    for j in range(len(results[i])):
        dt_object = datetime.fromtimestamp(results[i][j][1])
        results[i][j][1] = dt_object.hour*60 + dt_object.minute

# plot the data
for i in range(len(params)):
    x,y = zip(*results[i])
    plt.scatter(y, x)
    plt.xlabel("Sensor "+args.sensor)
    plt.ylabel(params[i])
    plt_file = "figures/" + args.sensor + "_" + params[i] + "_fit" + ".png"
    plt.savefig(plt_file)
    plt.clf()

#fit the data per parameter
for i in range(len(params)):
    clf = IsolationForest(random_state=0)
    X_train = np.array(results[i])
    clf.fit(X_train)
    file = args.sensor + "_" + params[i]
    with open("models/"+file,'wb') as f:
        pickle.dump(clf,f)
        