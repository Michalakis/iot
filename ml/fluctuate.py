import argparse as ap
import time
from utils.rules import RulesManagement

rm = RulesManagement()

parser = ap.ArgumentParser(description='Fluctuations')
parser.add_argument('-s', '--sensor', type=str, help='Sensor ID', default="0000000")
parser.add_argument('-t', '--type', type=int, help='1 for short, 2 for long', default=1)
parser.add_argument('-d', '--days', type=int, help='days back', default=7)

args = parser.parse_args()
      
now = int(time.time())
start = now - 3600*24*args.days
if args.type==1:
    rm.checkShortFluctuationsSensor(args.sensor, args.days)
elif args.type==2:
    cl = rm.checkSeasonalFluctuationsSensor(args.sensor, start, now)
else:
    print ("type must be 1 or 2")