import argparse as ap
import time
from copy import deepcopy
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
from sklearn.ensemble import IsolationForest
import pickle
from utils.sensors import SensorManagement

plt.rcParams['backend'] = "Qt4Agg"
sm = SensorManagement()
rng = np.random.RandomState(42)

parser = ap.ArgumentParser(description='Finding outliers')
parser.add_argument('-s', '--sensor', type=str, help='Sensor ID', default="0000000")
parser.add_argument('-t', '--time', type=float, help='number of hours back for data collection', default=1.0)
args = parser.parse_args()

      
# Load data for prediction 
end = int(time.time())
start = end - 3600*args.time
params, results = sm.getSensorDataAll(args.sensor, start, end)

results_raw = deepcopy(results)

# fix the timestamps to minutes in the day
for i  in range(len(params)):
    for j in range(len(results[i])):
        dt_object = datetime.fromtimestamp(results[i][j][1])
        results[i][j][1] = dt_object.hour*60 + dt_object.minute

#predict the data per parameter
for i in range(len(params)):
    file = args.sensor + "_" + params[i]
    with open("models/"+file,'rb') as f:
        clf = pickle.load(f)
    X_test = np.array(results[i])
    X_test_raw = np.array(results_raw[i])
    y_pred = clf.predict(X_test)
    y_pred_ok = []
    y_pred_nok = []
    for j in range(len(X_test_raw)):
        if y_pred[j]==1:
            y_pred_ok.append(X_test_raw[j])
        else:
            y_pred_nok.append(X_test_raw[j])
    y_pred_ok = np.array(y_pred_ok)
    y_pred_nok = np.array(y_pred_nok)
    
    # plot the line, the samples, and the nearest vectors to the plane
    plt.title("Sensor "+args.sensor)
    b1 = plt.scatter(y_pred_ok[:, 1], y_pred_ok[:, 0], c='white',
                     s=20, edgecolor='k')
    b2 = plt.scatter(y_pred_nok[:, 1], y_pred_nok[:, 0], c='red',
                     s=20, edgecolor='k')
    plt.axis('tight')
    plt.xlabel(time.ctime(start)+"-->"+time.ctime(end))
    plt.ylabel(params[i])
    plt.legend([b1, b2],
               ["normal",
                "outliers"],
               loc="upper left")
    plt_file = "figures/" + args.sensor + "_" + params[i] + "_predict_" + str(end) + ".png"
    plt.savefig(plt_file)
    plt.clf()