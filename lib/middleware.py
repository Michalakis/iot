from pprint import pprint
from pymongo import MongoClient
from bson import ObjectId
import time

### HARD CODES ###
TIME_RESTRICTION =  [{"month":"1-4,6,12","year":"","day":"15-30","weekday":"0-7","hour":"08:00-14:00,15:00-21:30"}]
TIME_RESTRICTION2 =  [{"month":"","year":"","day":"","weekday":"1-5","hour":"08:00-17:00"}]
TIME_RESTRICTION3 = [{"month":"","year":"","day":"","weekday":"1","hour":"17:00-18:00"}]
TIME_RESTRICTION_NULL = [{"month":"","year":"","day":"","weekday":"","hour":""}]
LOCATION_RESTRICTION = {"locationID":[],"locationClass":[]}
TYPE_THING_USER = 1
TYPE_THING_DEVICE = 2
TYPE_LOCATION_INDOORS = 1
TYPE_LOCATION_OUTDOORS = 2
TYPE_POSITION_ABSOLUTE = 1
TYPE_POSITION_RELATIVE = 2
TYPE_POSITION_CIRCLE = 1
TYPE_POSITION_POLY = 2
TYPE_PROFILE_THING = 1
TYPE_PROFILE_LOCATION = 2
TYPE_PROFILE_TIME = 3
TYPE_PROFILE_ACTIVITY = 4
TYPE_PROFILE_AUTH_ALLOW_ENTRY = 1
TYPE_PROFILE_AUTH_ALLOW_EXEC = 2
TYPE_PROFILE_ENVIRONMENTAL = 3
TYPE_PROFILE_RESULT_ACTIVITY = 1
TYPE_ACTIVITY_INSTANT = 1
TYPE_ACTIVITY_DURATIVE = 2
TYPE_SCHEDULE_MANUAL = 1
TYPE_SCHEDULE_DERIVED = 2
TYPE_LOCATION_THING_FIXED = 1
TYPE_LOCATION_THING_MOVABLE = 2
TYPE_ACTIVITY_ACTIVE = 1
TYPE_ACTIVITY_PASSIVE = 2
OBJECT_RELATIONSHIP_BELONGS = ObjectId('60802d69547576aedcf3c81d')
OBJECT_RELATIONSHIP_WORN = ObjectId('6130c7e060f158cb2531b0b9')
OBJECT_ACTIVITY_MEASUREMENT = ObjectId('6130c9ff60f158cb2531b0bf')
OBJECT_ACTIVITY_GPS = ObjectId('6130d3bb1a92e7eb4d3a8e6b')
OBJECT_ACTIVITY_BLE = ObjectId('608a645e0a9acef928fb67e5')
OBJECT_PARAMETER_COORDINATES = ObjectId('6130c7a760f158cb2531b0b8')
OBJECT_THING_CLASS_TURNED_ON = ObjectId('608bcdee13424eb6335f24bb')
OBJECT_THING_CLASS_TURNED_OFF = ObjectId('608bcdff13424eb6335f24bc')

NO = 0
YES = 1
CONDIFENCE_THRESHOLD = 0.5
MINS_LOCATION_OLDNESS = 15
LOCATION_OLDNESS_THRESHOLD = 0.3
ENVIRONMENT_OLDNESS_THRESHOLD = 0.2
LOG = True

GPS_COEFF = 1.0
DEFAULT_COEFF = 2.0
LOCATION_COEFF = 3.0
DISTANCE_COEFF = 20

from datetime import datetime
import math
import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from haversine import haversine

def timestamp():
    return int(datetime.timestamp(datetime.now()))

def typeOldness(diff, coeff):
    hours = diff/3600
    return math.exp(-hours/coeff)

def calcAverage(a, b):
    s1 = 0
    s2 = 0
    p = 0
    for i in range(len(a)):
        s1 += a[i]*b[i]**3
        s2 += b[i]**4
        p += b[i]**3
    f1 = s1/p
    f2 = s2/p
    return f1, f2

def distance(x, y):
    if x==None or y==None or len(x)!=2 or len(y)!=2:
        return math.inf
    d2r = 0.0174532925199433
    dlong = (y[0] - x[0]) * d2r
    dlat = (y[1] - x[1]) * d2r
    a = pow(math.sin(dlat/2.0), 2) + math.cos(x[1]*d2r) * math.cos(y[1]*d2r) * pow(math.sin(dlong/2.0), 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = 6367 * c
    if d == 0:
        d = 0.0000001 # never let distance 0
    return d * 1000

def inPoly(point, poly):
    polygon = Polygon(poly) # create polygon
    pt = Point(point[0], point[1]) # create point
    flag = polygon.contains(pt)
    if flag:
        center = polygon.centroid
        dist = distance(point, [center.x, center.y])
    else:
        dist_coords = polygon.distance(pt)
        dist = haversine((dist_coords, 0),(0, 0))*1000
    return flag, dist

def calcConfidenceLocation(dist):
    return math.exp(-dist/DISTANCE_COEFF)

def transformTimeElement(string):
    if ':' in string:
        elements = string.split(":")
        return int(elements[0])*60 + int(elements[1])
    else:
        return int(string)

def checkTimeElement(string, value, hour_flag = False):
    flag = False
    if string == "":
        return True
    items = string.split(",")
    for item in items:
        if '-' not in item:  #check for stand alone 
            if transformTimeElement(item) == value:
                flag = True
                break
        else:                 #check for ranges 
            elements = item.split("-")
            if len(elements)!=2:
                print ("error on month criterion")
                continue
            if value >= transformTimeElement(elements[0]) and value <= transformTimeElement(elements[1]):
                flag = True
                break
    return flag

def resolveTime(time_array):
    now = datetime.now()
    flag = True
    for time in time_array:
        flag *= checkTimeElement(time["month"], now.month) 
        flag *= checkTimeElement(time["year"], now.year)
        flag *= checkTimeElement(time["day"], now.day)
        flag *= checkTimeElement(time["weekday"], now.weekday())
        flag *= checkTimeElement(time["hour"], now.hour*60 + now.minute, hour_flag=True)
    return flag

def common_member(a, b):
    a_set = set(a)
    b_set = set(b)
    if (a_set & b_set):
        return True 
    else:
        return False
    
def checkLocationOldeness(time):
        if timestamp() - int(time) > MINS_LOCATION_OLDNESS * 60:
            return True
        else:
            return False
        
def log(message):
    if LOG:
        print (message)
        


class EntityManagement:
    def __init__(self):
        self.db = MongoClient('mongodb+srv://kmichalakis:GondoliN6!@cluster0-vaaym.mongodb.net/test?retryWrites=true&w=majority').iot 

    #returns a list of entitues of a class which satisfy criteria
    #latest is field that computes latest entry
    #argv is couples of (field, criteria)
    def getEntities(self, c, *argv, latest="", nonempty="", sort="", sortorder="DESC"):
        if len(argv)%2 == 1:
            log ("Wrong arguments")
            return
        query = {}
        for i in range(0, len(argv), 2):
            query[argv[i]] = argv[i+1]
        if nonempty!="":
            query[nonempty]= { "$exists": True, "$ne": [] }
        if latest=="":
            if sort=="":
                return list(self.db[c].find(query))
            elif sortorder=="ASC":
                return list(self.db[c].find(query).sort(sort,1))
            else:
                return list(self.db[c].find(query).sort(sort,-1))
        else:
            return list(self.db[c].find(query).sort(latest,-1).limit(1))
        
    def getEntityID(self, c, *argv, latest="", nonempty="", sort="", sortorder="DESC"):
        obj = self.getEntities(c, *argv, latest="", nonempty="", sort="", sortorder="DESC")
        return obj[0]["_id"]
    
    def addEntity(self, c, item):
        _id = self.db[c].insert_one(item)
        return _id.inserted_id
    
    def editEntity(self, c, uid, field, value):
        self.db[c].update_one({ "_id": uid },{"$set": { field: value }})
    
    def add(self, c, *argv):
        if len(argv)%2 == 1:
            log ("Wrong arguments")
            return
        query = {}
        for i in range(0, len(argv), 2):
            query[argv[i]] = argv[i+1]
        return self.addEntity(c, query)
    
    def delete(self, c, uid):
        self.db[c].delete_one({"_id":uid})
        
        
    ########################
    def getRelatedThings(self, thingID):
        things = [thingID]
        related_things = self.getEntities("relationship", "thingID", thingID, "relationshipID", OBJECT_RELATIONSHIP_BELONGS)
        related_things.extend(self.getEntities("relationship", "thingID", thingID, "relationshipID", OBJECT_RELATIONSHIP_WORN))
        for related_thing in related_things:
            things.append(related_thing["targetID"])
        related_things = self.getEntities("relationship", "targetID", thingID, "relationshipID", OBJECT_RELATIONSHIP_BELONGS)
        related_things.extend(self.getEntities("relationship", "targetID", thingID, "relationshipID", OBJECT_RELATIONSHIP_WORN))
        for related_thing in related_things:
            things.append(related_thing["thingID"])
        return things
        
    ### LOCATION FUNCTIONS ###
    
    #returns all the classes of a location (and the super classes)
    def getLocationClasses(self, locationID):
        location_classes = self.getEntities("location", "_id", locationID)[0]["class"]
        location_classes_all = []
        for location_class in location_classes:
            location_classes_all.append(location_class)
            obj = self.getEntities("location_class", "_id", location_class)[0] 
            if "belongs" in obj:
                location_classes_all.append(obj["belongs"]) 
        return location_classes_all

    #updates the found location, by archiving the old entries first
    def updateLocationThing(self, thingID, locationID, confidence, now, type_=TYPE_LOCATION_THING_MOVABLE):
        #first find old entries and move then to archive
        location_things = self.getEntities("location_thing", "thingID", thingID)
        for location_thing in location_things:
            self.add("location_thing_archive", "thingID", location_thing["thingID"], "locationID", location_thing["locationID"], "confidence", 
                   location_thing["confidence"], "type", location_thing["type"], "timestamp", location_thing["timestamp"])
            self.delete("location_thing", location_thing["_id"])
        #now add new entry
        self.add("location_thing", "thingID", thingID, "locationID", locationID, "confidence", confidence, "type", type_, "timestamp", now)

        
    #manually sets a location of a thing   
    def setLocationThing(self, thingID, locationID, type_=TYPE_LOCATION_THING_FIXED):
        now = timestamp()
        confidence = 1.0
        #update location for thing
        self.updateLocationThing(thingID, locationID, confidence, now, type_)
        #update location for things related (thingID is main)
        related_things = self.getEntities("relationship", "thingID", thingID, "relationshipID", OBJECT_RELATIONSHIP_BELONGS)
        related_things.extend(self.getEntities("relationship", "thingID", thingID, "relationshipID", OBJECT_RELATIONSHIP_WORN))
        for related_thing in related_things:
            self.updateLocationThing(related_thing["targetID"], locationID, confidence, now)
        #update location for things related (thingID is target)
        related_things = self.getEntities("relationship", "targetID", thingID, "relationshipID", OBJECT_RELATIONSHIP_BELONGS)
        related_things.extend(self.getEntities("relationship", "targetID", thingID, "relationshipID", OBJECT_RELATIONSHIP_WORN))
        for related_thing in related_things:
            self.updateLocationThing(related_thing["thingID"], locationID, confidence, now)
     
    
    def getGPSThing(self, thingID):
        measurement = self.getEntities("action", "activityID", OBJECT_ACTIVITY_GPS, "thingID", thingID, latest="timestamp")
        if len(measurement)>0:
            return measurement[0]["value"], measurement[0]["timestamp"]
        #no gps measurement for the thing, find if owned things have gps
        related_things = self.getEntities("relationship", "targetID", thingID, "relationshipID", OBJECT_RELATIONSHIP_BELONGS)
        related_things.extend(self.getEntities("relationship", "targetID", thingID, "relationshipID", OBJECT_RELATIONSHIP_WORN))
        for related_thing in related_things:
            measurement = self.getEntities("action", "activityID", OBJECT_ACTIVITY_GPS, "thingID", related_thing["thingID"], latest="timestamp")
            if len(measurement)>0:
                return measurement[0]["value"], measurement[0]["timestamp"]
        return None, 0
            
        
    #calculates the location of a thing, based on GPS measurements 
    def calcLocationThing(self, thingID):
        log ("Calculating location for " + str(thingID))
        thing = self.getEntities("thing", "_id", thingID)
        if len(thing)==0:
            log ("Wrong thing ID")
            return 0,0
        now = timestamp()
        measurement = self.getEntities("action", "activityID", OBJECT_ACTIVITY_GPS, "thingID", thingID, latest="timestamp")
        if len(measurement)==0: #no gps measurement for the thing, find if owned things have gps
            a = b = 0
            #find the things that have relationship Belong or Is Worn with the current object and update location
            related_things = self.getEntities("relationship", "targetID", thingID, "relationshipID", OBJECT_RELATIONSHIP_BELONGS)
            related_things.extend(self.getEntities("relationship", "targetID", thingID, "relationshipID", OBJECT_RELATIONSHIP_WORN))
            for related_thing in related_things:
                a, b = self.calcLocationThing(related_thing["thingID"])
            if a == 0:
                log("No location found")
            return a,b
        coords = measurement[0]["value"]
        oldness = typeOldness(now - int(measurement[0]["timestamp"]), LOCATION_COEFF)
        now = timestamp()
        min_dist = 99999999
        loc_found = 0
        found_in = False;
        confidence = 1.0 * oldness
        locations = self.getEntities("location", "type", TYPE_LOCATION_OUTDOORS, "position", TYPE_POSITION_ABSOLUTE)
        for location in locations:
            if location["coords_type"]==TYPE_POSITION_CIRCLE:
                dist = distance(coords, location["coords"])
                if location["radius"]>=dist and min_dist>dist:  #check if in radius and min dist
                    min_dist = dist
                    loc_found = location["_id"]
                    found_in = True  #found in radius, so no need to check for proximity outside of radius
                elif location["radius"]<dist and not found_in and min_dist>dist:   #check if not in radius, but min _dist
                    min_dist = dist
                    loc_found = location["_id"]
            elif location["coords_type"]==TYPE_POSITION_POLY:
                inpoly, dist = inPoly(coords, location["coords"])
                if inpoly and min_dist>dist:
                    min_dist = dist
                    loc_found = location["_id"]
                    found_in = True
                elif not inpoly and not found_in and min_dist>dist:
                    min_dist = dist
                    loc_found = location["_id"] 
        if not found_in: # not found in poly/circle, but maybe close. calculate confidence
            confidence = calcConfidenceLocation(min_dist) * oldness
            if confidence<CONDIFENCE_THRESHOLD:  #if calculate is below threshold, then ignore that finding
                loc_found = 0
        print (found_in, confidence)
        self.updateLocationThing(thingID, loc_found, confidence, now)
        #find the things that have relationship Belong or Is Worn with the current object and update location
        related_things = self.getEntities("relationship", "thingID", thingID, "relationshipID", OBJECT_RELATIONSHIP_BELONGS)
        related_things.extend(self.getEntities("relationship", "thingID", thingID, "relationshipID", OBJECT_RELATIONSHIP_WORN))
        for related_thing in related_things:
            self.updateLocationThing(related_thing["targetID"], loc_found, confidence, now)
        log("Location found: " + str(loc_found))
        return loc_found, confidence
    
    
    #returns the location of a thing based on db
    #force parameter forces an update of location
    def findLocationThing(self, thingID, force=False):
        location_things = self.getEntities("location_thing", "thingID", thingID, latest="timestamp")
        #if no or old entry, check if a new location can be calculated
        if len(location_things)==0 or force or \
           (checkLocationOldeness(location_things[0]["timestamp"]) and location_things[0]["type"]==TYPE_LOCATION_THING_MOVABLE): #only applicable to movable objects
            log ("No or old location for "+ str(thingID))
            self.calcLocationThing(thingID)
            location_things = self.getEntities("location_thing", "thingID", thingID, latest="timestamp")
            if len(location_things)==0: #if still no entry, check whether belonging things can be located
                #find the things that have relationship Belong or Is Worn with the current object and update location
                related_things = self.getEntities("relationship", "targetID", thingID, "relationshipID", OBJECT_RELATIONSHIP_BELONGS)
                related_things.extend(self.getEntities("relationship", "targetID", thingID, "relationshipID", OBJECT_RELATIONSHIP_WORN))
                for related_thing in related_things:
                    a, b = self.calcLocationThing(related_thing["thingID"])
                location_things = self.getEntities("location_thing", "thingID", thingID, latest="timestamp")
                if len(location_things)==0: #if yet no entry, no location can be found return zero
                    return 0,0
        return location_things[0]["locationID"], location_things[0]["confidence"]
        
    
    def getThingsInLocationList(self, locationID):
        #find things directly in location
        location_things = self.getEntities("location_thing", "locationID", locationID)
        #find things located in locations that belong to this and share environment
        shared_locations = self.getEntities("location", "belongs", locationID, "belongs_environment", YES)
        for shared_location in shared_locations:
            shared_things = self.getEntities("location_thing", "locationID", shared_location["_id"])
            location_things.extend(shared_things)
        return location_things
    
    
    #find all things in a location - use already stored location_things    
    def findThingsInLocation(self, locationID):
        location_things = self.getThingsInLocationList(locationID)
        #for each thing found, check if location is old and recalc
        for location_thing in location_things:
            if  location_thing["type"]==TYPE_LOCATION_THING_MOVABLE and checkLocationOldeness(location_thing["timestamp"]):
                log ("No or old location for "+ str(location_thing["thingID"]))
                self.calcLocationThing(location_thing["thingID"])
        location_things = self.getThingsInLocationList(locationID)   
        now = timestamp()
        things = []    
        for location_thing in location_things:
            thing = self.getEntities("thing", "_id", location_thing["thingID"])
            if len(thing)==0:
                log ("unexpected error in findThingsInLocation")
                continue
            obj = {}
            obj["thingID"] = thing[0]["_id"]
            if location_thing["type"]==TYPE_LOCATION_THING_FIXED:  #if fixed object, confidence is always 1.0
                confidence = 1.0
            else:   #if movable object, confidence is oldness * confidence of entry
                oldness = typeOldness(now - int(location_thing["timestamp"]), LOCATION_COEFF)
                confidence = location_thing["confidence"] * oldness
            obj["confidence"] = confidence
            things.append(obj)
        return things
    
    
    #calculate the environment of a location and store it at DB
    def updateEnvironment(self, locationID):
        now = timestamp()
        things = self.findThingsInLocation(locationID)
        env = []
        for thing in things:
            #each device can sense only one parameter at most
            parameter = self.getEntities("parameter_thing", "thingID", thing["thingID"])
            if len(parameter)==0:  #this thing is not sensor, so skip
                continue
            paramID = parameter[0]["parameterID"]
            #get measurement
            measurement = self.getEntities("action", "activityID", OBJECT_ACTIVITY_MEASUREMENT, "thingID", thing["thingID"], latest="timestamp")
            if len(measurement)==0: #this sensor has no measurements
                continue
            oldness = typeOldness(now - int(measurement[0]["timestamp"]), DEFAULT_COEFF)
            confidence = oldness * thing["confidence"]
            found = next((i for i, item in enumerate(env) if item["param"] == paramID), None)
            if found == None:
                obj = {}
                obj["param"] = paramID
                obj["confidence"] = [confidence]
                obj["value"] = [measurement[0]["value"]]
                env.append(obj)
            else:
                env[found]["value"].append (measurement[0]["value"])
                env[found]["confidence"].append (confidence)
        #make an entry for the environment
        _id = self.add("environment", "locationID", locationID, "timestamp", now)
        for index, env_item in enumerate(env):
            if len(env_item["value"])==1:  #if only one value, store to db
                value = env_item["value"][0]
                confidence = env_item["confidence"][0]
            else:  #if more values calculate the average
                value, confidence = calcAverage(env[index]["value"], env[index]["confidence"])
                env[index]["value"] = value
                env[index]["confidence"] = confidence 
            #for each param make a DB entry
            self.add("environment_detail", "environmentID", _id, "paramID", env_item["param"], "value", value, "confidence", confidence)
    
    
    #returns the environmental details of a location
    def findEnvironment(self, locationID, force=False):
        now = timestamp()
        env = self.getEntities("environment", "locationID", locationID, latest="timestamp")
        if len(env)==0 or force:  # if no environment or force para,, calculate one
            self.updateEnvironment(locationID)
            env = self.getEntities("environment", "locationID", locationID, latest="timestamp")
        oldness = typeOldness(now - env[0]["timestamp"], GPS_COEFF)
        if oldness < ENVIRONMENT_OLDNESS_THRESHOLD:  #ignore too old environment
            self.updateEnvironment(locationID)
            env = self.getEntities("environment", "locationID", locationID, latest="timestamp")
        env_details = self.getEntities("environment_detail", "environmentID", env[0]["_id"])
        for item in env_details:
            item.pop("_id")
            item.pop("environmentID")
        return env_details
                
    
    ##### THINGS ##########
    
    def checkLocationRestriction(self, obj, locationID, location_classes):
        if len(obj["locationID"])>0 and locationID in obj["locationID"]:
            return True
        elif len(obj["locationClass"])>0 and common_member(location_classes, obj["locationClass"]):
            return True
        elif len(obj["locationID"])==0 and len(obj["locationClass"])==0:
            return True
        return False
    
    #check whether a thing at a location has a role (or which roles it has)
    def checkThingRoles(self, thingID, locationID, roles=[]):
        thing_roles = self.getEntities("role", "thingID", thingID)
        location_classes = self.getLocationClasses(locationID)
        thing_roles_applied = []
        for thing_role in thing_roles:
            #check location
            if not self.checkLocationRestriction(thing_role["location"], locationID, location_classes):
                continue
            #check time
            if not resolveTime(thing_role["time"]):
                continue
            thing_roles_applied.append(thing_role["roleID"])
        if len(roles)==0:
            return thing_roles_applied
        else:
            return common_member(thing_roles_applied, roles)
    
    
    #returns the super thing of a thing (i.e. where it belongs or is worn)
    def getThingSuper(self, thingID):
        super_thing = self.getEntities("relationship", "thingID", thingID, "relationshipID", OBJECT_RELATIONSHIP_BELONGS)
        if len(super_thing)!=0:
            return self.getThingSuper(super_thing[0]["targetID"])
        super_thing = self.getEntities("relationship", "thingID", thingID, "relationshipID", OBJECT_RELATIONSHIP_WORN)
        if len(super_thing)!=0:
            return self.getThingSuper(super_thing[0]["targetID"])  
        return thingID
    
    
    #returns a list of the thing_class and all its superclasses
    def getAllClassesThing(self, classID):
        ret = [classID]
        obj = self.getEntities("thing_class", "_id", classID)
        if len(obj)==0:
            return ret
        if "belongs" in obj[0]:
            ret.extend(self.getAllClassesThing(obj[0]["belongs"]))
        return ret
    
    
    #returns if a thing belongs to a list of classes 
    def checkThingClass(self, thingID, classID):
        if len(classID)==0:
            return True
        obj = self.getEntities("thing", "_id", thingID)
        if len(obj)==0:
            return False
        if "class" not in obj[0] or len(obj[0]["class"])==0:
            return False
        found = False
        for class_ in obj[0]["class"]:
            all_class = self.getAllClassesThing(class_)
            if common_member(all_class, classID):
                found = True
        return found
    
    
    #finds all pois that are close to a thing
    def findPoisThing(self, thingID, classes=[]):
        now = timestamp()
        pois = []
        #first check for BLE measurements
        things = self.getRelatedThings(thingID)
        for thing in things:
            ble_actions = self.getEntities("action", "activityID", OBJECT_ACTIVITY_BLE, "thingID", thing)
            for ble_action in ble_actions:
                score = typeOldness(now - ble_action["timestamp"], GPS_COEFF)
                if score < LOCATION_OLDNESS_THRESHOLD:  #ignore too old measurements
                    continue
                all_things = ble_action["thingID"]
                all_things.remove(thing)
                poi_thing = self.getThingSuper(all_things[0])
                if not self.checkThingClass(poi_thing, classes):  #do not add if not in thing classes requested
                    continue
                #check if same ble target already exists and keep the latest
                if any(d['thingID'] == poi_thing for d in pois): #found
                    for d in pois:
                        if d["thingID"]==poi_thing:
                            #check now if new has better score
                            if score > d["score"]:
                                d["score"] = score
                                d["distance"] = ble_action["value"]
                            #if not then do nothing and keep old record
                else: #if target thing does not exist, just add a new record
                    poi = {"thingID": poi_thing, "score": score, "distance": ble_action["value"]}
                    pois.append(poi)
        #now check for GPS measurements
        thing_coords, dummy = self.getGPSThing(thingID)
        if thing_coords != None:  #check if thing has GPS measurement
            locationID, dummy = self.findLocationThing(thingID, force = True)  #now find its location
            if locationID!=0:   # if it has a location
                things = self.findThingsInLocation(locationID)  #get all things in location
                for thing in things:
                    poi_coords, poi_timestamp = self.getGPSThing(thing["thingID"])
                    thingID_used = self.getThingSuper(thing["thingID"])
                    if thingID_used==thingID: #ignore self 
                        continue
                    if not self.checkThingClass(thingID_used, classes):  #do not add if not in thing classes requested
                        continue
                    #check if same ble target already exists and keep the latest
                    if any(d['thingID'] == thingID_used for d in pois): #found
                        for d in pois:
                            if d["thingID"]==thingID_used: 
                                if poi_coords != None:  #found and new record has coords
                                    dist = distance(poi_coords, thing_coords)
                                    score = typeOldness(now - poi_timestamp, GPS_COEFF) 
                                    #check now if new has better score
                                    if score > d["score"]:
                                        d["score"] = score
                                        d["distance"] = dist
                                else:  #found and new record has no coords, so ignore it
                                    pass 
                    else:  #if target thing does not exist, just add a new record            
                        if poi_coords == None:  #if no gps for that thing, just add at max value and min score
                            poi = {"thingID": thingID_used, "score": 0.0, "distance": 1000}
                        else:
                            dist = distance(poi_coords, thing_coords)
                            score = typeOldness(now - poi_timestamp, GPS_COEFF) 
                            poi = {"thingID": thingID_used, "score": score, "distance": dist}
                        pois.append(poi)
                        

        pois = sorted(pois, key = lambda i: i['score'], reverse = True)
        return pois
                         
        
    def updateThingClass(self, thingID, classID):
        obj = self.getEntities("thing", "_id", thingID)[0]
        thing_classes = obj["class"]
        #add class if not there
        if classID not in thing_classes:
            thing_classes.append(classID) 
        #remove exclusive classes
        obj = self.getEntities("thing_class", "_id", classID)[0]
        if "exclusive" in obj:
            for item in obj["exclusive"]:
                if item in thing_classes:
                    thing_classes.remove(item)
        self.editEntity("thing", thingID, "class", thing_classes)
    
    ##### ACTIVITIES ##########
    
    #check whether a thing can execute an activity with another thing
    def findActivityExecution(self, thingID, targetID, activityID):
        locationID, conf = self.findLocationThing(targetID)
        location_classes = self.getLocationClasses(locationID)
        #find profile activities
        profile_activities = self.getEntities("profile_activity", "activityID", activityID)
        profiles = []
        for profile_activity in profile_activities:
            #check if profile is valid 
            #for location
            if not self.checkLocationRestriction(profile_activity["location"], locationID, location_classes):
                continue
            if not resolveTime(profile_activity["time"]):
                continue
            profiles.append(profile_activity["profileID"])
        if len(profiles)==0: #no profiles, execute is allowed
            found = True
        else:
            found = False
            for profile in profiles:  #check each rule for execute rules
                profile_rules = self.getEntities("profile_rule", "profileID", profile, "type", TYPE_PROFILE_AUTH_ALLOW_EXEC)
                for profile_rule in profile_rules:
                    #check if user is in thingID array
                    if thingID in profile_rule["thingID"]:
                        found = True
                    #check if user has one of allowed roles
                    if self.checkThingRoles(thingID, locationID, profile_rule["roleID"]):
                        found = True
        return found
    
    #find activities that are available with the things in the location
    #if passive is true, then also show passive activities
    def findServices(self, locationID, passive=False):
        services = []
        location_classes = self.getLocationClasses(locationID)
        things = self.findThingsInLocation(locationID)
        for thing in things:
            activities = []
            #for each thing find the activities
            if passive:
                activities_thing = self.getEntities("activity_thing", "thingID", thing["thingID"])
            else:            
                activities_thing = self.getEntities("activity_thing", "thingID", thing["thingID"], "type", TYPE_ACTIVITY_ACTIVE)
            print (activities_thing)
            #for each activity check for profile
            for activity_thing in activities_thing:
                check = True
                profile_activities = self.getEntities("profile_activity", "activityID", activity_thing["_id"])
                for profile_activity in profile_activities:
                    #check if profile is valid for time and location
                    if not self.checkLocationRestriction(profile_activity["location"], locationID, location_classes) or \
                       not resolveTime(profile_activity["time"]):
                        check=False
                        break
                if check:
                    activities.append(activity_thing["_id"])
            services.append({"thingID":thing["thingID"], "activityID":activities})
        return services
    
    #find activities that a thing can execute in a location. if location is None, then check the current location of thing
    #it can be used to check what activities a thing can perform in a location (even if the thing is not there at the moment)
    #if passive is true, then also show passive activities, else show only active activities
    #force parameter forces to recalc position of thing
    def findServicesThing(self, thingID, locationID=None, passive=False, force=False):
        if locationID==None or force:  #find services at the place of the 
            locationID, conf = self.findLocationThing(thingID, force=force)
        services_allowed = []
        if locationID == 0:
            return services_allowed
        services = self.findServices(locationID, passive)
        for service in services:
            obj = {}
            obj["thingID"] = service["thingID"]
            obj["activityID"] = []
            for activity in service["activityID"]:
                if self.findActivityExecution(thingID, service["thingID"], activity):
                    obj["activityID"].append(activity)
            services_allowed.append(obj)
        return services_allowed
    
    
    #### PROFILES AND RULES ####
    
    #checks the rule of a profile and evaluates if it is true or not
    def evalRule(self, profile_rule, locationID):
        if profile_rule["type"] == TYPE_PROFILE_ENVIRONMENTAL:
            paramID = profile_rule["paramID"]
            rule = profile_rule["rule"]
            env_params = self.findEnvironment(locationID)
            confidence = 0
            for env_param in env_params:
                if env_param["paramID"] == paramID:
                    env_param_value = env_param["value"]
                    confidence = env_param["confidence"]
                    break;
            if confidence == 0: #we did not find a param at location environment
                return None, 0
            return not eval(str(str(env_param_value)+rule)), confidence
    
    
    #execute the results of a profile           
    def executeProfileResults(self, profileID, locationID):
        profile_results = self.getEntities("profile_result", "profileID", profileID)
        for profile_result in profile_results:
            if profile_result["type"] == TYPE_PROFILE_RESULT_ACTIVITY:
                things = self.findThingsInLocation(locationID)
                for thing in things:
                    activities_thing = self.getEntities("activity_thing", "activityID", profile_result["activityID"], "thingID", thing["thingID"])
                    if len(activities_thing) > 0:
                        self.performAction(thing["thingID"], profile_result["activityID"], profile_result["value"])

    #perform an action                  
    def performAction(self, thingID, activityID, value):
        activity = self.getEntities("activity", "_id", activityID)[0]
        #check if required class is present at object
        if "class_required" in activity:
            thing = self.getEntities("thing", "_id", thingID)[0]
            if activity["class_required"] not in thing["class"]:
                log("class not present")
                return
        #update class if needed
        if "class_received" in activity:
            self.updateThingClass(thingID, activity["class_received"])
        log ("Performing activity " + str(activityID) + " with thing " + str(thingID) + " at value " + str(value))
        self.add("action", "activityID", activityID, "thingID", [thingID], "timestamp", timestamp(), "value", value)
        #TODO check collisions
        #TODO add metacontext
        
        
    #check profile results and execute results for a thing in a location
    def checkProfileThing(self, thingID, locationID = 0):
        if locationID == 0:
            locationID, conf = self.findLocationThing(thingID)
        location_classes = self.getLocationClasses(locationID)
        thing_profiles = self.getEntities("profile_thing", "thingID", thingID)
        for thing_profile in thing_profiles:
            #check location
            if not self.checkLocationRestriction(thing_profile["location"], locationID, location_classes):
                continue
            #check time
            if not resolveTime(thing_profile["time"]):
                continue
            #check rules of profile
            check = True
            min_conf = 1.0
            profile_rules = self.getEntities("profile_rule", "profileID", thing_profile["profileID"])
            for profile_rule in profile_rules:
                rule, conf = self.evalRule(profile_rule, locationID)
                if rule == None:
                    log("this rule cannot be concluded because of lacking data")
                    check = False
                elif rule == False:
                    check = False
                if conf < min_conf:
                    min_conf = conf
            if not check:
                continue
            if min_conf < thing_profile["aggressive"]:
                log("profile applicable but confidence not enough")
                continue
            log ("profile applicable with confidence " + str(min_conf))
            #execute results
            self.executeProfileResults(thing_profile["profileID"], locationID)
            
    def checkProfileThingLocation(self, locationID = 0):
        things = self.findThingsInLocation(locationID)
        for thing in things:
            self.checkProfileThing(thing["thingID"], locationID)