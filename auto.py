import argparse as ap
from utils.sensors import SensorManagement
from utils.rules import RulesManagement
from pprint import pprint
import time
from datetime import datetime
rm = RulesManagement()

parser = ap.ArgumentParser(description='Checking Rules')
parser.add_argument('-t', '--hours', type=float, help='hours before start', default=1.0)
parser.add_argument('-s', '--step', type=float, help='minutes to trigger next check', default=5)
parser.add_argument('-m', '--museum', type=int, help='mudeum id to check', default=1)
parser.add_argument('-l', '--log', type=str, help='log file', default="")
args = parser.parse_args()


now = int(time.time())
starttime = now - 3600*args.hours
endtime = now

while (1):
    actions = rm.checkRulesMuseum(args.museum, starttime, endtime, args.log)
    print ("Waiting for ", args.step, " minutes")
    time.sleep(args.step*60)
    now = int(time.time())
    starttime = now - args.step*60
    endtime = now
    
    
    